import React from "react";
import AdminSlider from "./AdminSlider";
import { Link } from "react-router-dom";
export default class bookingVehicle extends React.Component
{
    render()
    {
        return<div>
            <div className="container-fluid bg-secondary min-vh-100">
                <div className="row">
                    <div className="col-2 bg-white vh-100">
                        <AdminSlider />
                    </div>
                    <div className="col-10 ">
                    <h1 style={{background:'#8a0dfd17',color:'white',textAlign:'center',padding:'20px'}}>booking Vehicle</h1>
            <table class="table  bg-white rounded">
                <thead>       
                    <tr>
                        <th scope="col">S.No</th>
                        <th scope="col">Vehicle Id</th>
                        <th scope="col">Vehicle Category</th>
                        <th scope="col">Booking</th>
                        <th scope="col">Price/Km</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                        <th scope="row">1</th>
                        <td>101</td>
                        <td>Tempo</td>
                        <td>Booked</td>
                        <td>49Rs</td>
                        <td><button className="btn btn-danger">Remove</button></td>
                    </tr>
                    <tr>
                    <th scope="row">2</th>
                        <td>102</td>
                        <td>Tata Intra</td>
                        <td>Pending</td>
                        <td>49Rs</td>
                        <td><button className="btn btn-danger">Remove</button></td>
                    </tr>
                <tr>
                        <th scope="row">3</th>
                        <td>103</td>
                        <td>Cargo</td>
                        <td>Completed</td>
                        <td>99Rs</td>
                        <td><button className="btn btn-danger">Remove</button></td>
                    </tr>
                    <tr>
                    <th scope="row">4</th>
                        <td>104</td>
                        <td>Prima</td>
                        <td>Cancled</td>
                        <td>149Rs</td>
                        <td><button className="btn btn-danger">Remove</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
        </div>
        </div>
        </div>
    }
}