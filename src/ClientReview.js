import { Component } from "react";

export default class ClientReview extends Component {
    render() {
        return <div>
            <div className="container-xxl py-5 wow fadeInUp" data-wow-delay="0.1s">
                <div className="container py-5">
                    <div className="text-center">
                        <h6 className="text-secondary text-uppercase">Testimonial</h6>
                        <h1 className="mb-0">Our Clients Say!</h1>
                    </div>
                    <form>
  <div className="mb-3">
    <label for="exampleInputEmail1" className="form-label">Email address</label>
    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required/>
    <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
  </div>
  
  <div className="mb-3">
    <label for="validationTextarea" className="form-label">Textarea</label>
    <textarea className="form-control is-invalid" id="validationTextarea" placeholder="FeedBack" required></textarea>
    <div className="invalid-feedback">
      Please enter a message in the textarea.
    </div></div>  <div className="mb-3 form-check">
    <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
    <label className="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" className="btn btn-primary">Submit</button>
</form>               </div>
            </div>
        </div>
    }
}