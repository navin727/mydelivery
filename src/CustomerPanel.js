import React from "react";
import { Link } from "react-router-dom";
export default class CustomerPanel extends React.Component {
    render() {
        return <div style={{background:'#8a0dfd17',color:'white',padding:'50px'}} className="mt-3 mx-5">
            <h1>Booking Form</h1>
            <pre>
                <form className="form-group">
                    <div className="row">
                        <div className="col-md-6 col-lg-6">
                            <input type="text" className="form-control" placeholder="Customer Name" />
                        </div>
                        <div className="col-md-6 col-lg-6">
                            <input type="text" className="form-control" placeholder="Mobile Number" />
                        </div>
                    </div><br />
                    <div className="row">
                        <div className="col-md-6 col-lg-6">
                            <input type="email" className="form-control" placeholder="Email" />
                        </div>
                        <div className="col-md-6 col-lg-6">
                            <input type="date" className="form-control" placeholder="Booking" />
                        </div>
                    </div><br />
                    <div className="row">
                        <div className="col-md-6 col-lg-6">
                            <input type="text" className="form-control" placeholder="Pickup Address" />
                        </div>
                        <div className="col-md-6 col-lg-6">
                            <input type="text" className="form-control" placeholder="Destination Address" />
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-md-6 col-lg-6">
                            <input type="time" className="form-control" placeholder="Pickup Time" />
                        </div>
                        <div className="col-md-6 col-lg-6">
                            <input type="text" className="form-control" placeholder="Goods Weight" />
                        </div>
                    </div>
                    <br/>
                    
                    <div className="row text-center">
                        <div className="col-md-12 col-lg-12">
                            <button className="btn-lg btn-success mb-2 ">submit</button>
                        </div>
                    </div>
                </form>
            </pre>
        </div>
    }
}