import React from "react";
import AdminSlider from "./AdminSlider";
import { Link } from "react-router-dom";
export default class payment extends React.Component
{
    render()
    {
        return<div>
            <div className="container-fluid bg-secondary min-vh-100">
                <div className="row">
                    <div className="col-2 bg-white vh-100">
                        <AdminSlider />
                    </div>
                    <div className="col-10 ">
            <h1 style={{background:'#8a0dfd17',color:'white',textAlign:'center',padding:'20px'}}>Payment Status</h1>
            <br/>
            <table class="table  bg-white rounded">
                <thead>       
                    <tr>
                        <th scope="col">S.No</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Vehicle Name</th>
                        <th scope="col">Total Amount</th>
                        <th scope="col">Paid</th>
                        <th scope="col">Remaining</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                        <th scope="row">1</th>
                        <td>Neeraj</td>
                        <td>Prima</td>
                        <td>50000</td>
                        <td>10000</td>
                        <td>40000</td>
                        <td>Booked</td>
                    </tr>
                    <tr>
                    <th scope="row">2</th>
                        <td>Adarsh</td>
                        <td>Cargo</td>
                        <td>40000</td>
                        <td>10000</td>
                        <td>30000</td>
                        <td>Pending</td>
                    </tr>
                <tr>
                        <th scope="row">3</th>
                        <td>Navin</td>
                        <td>Tata Intra</td>
                        <td>20000</td>
                        <td>20000</td>
                        <td>0</td>
                        <td>Completed</td>
                    </tr>
                    <tr>
                    <th scope="row">4</th>
                        <td>Vaibhav</td>
                        <td>Tempo</td>
                        <td>3000</td>
                        <td>0</td>
                        <td>0</td>
                        <td>Cancled</td>
                        </tr>
                </tbody>
            </table>
        </div>
        </div>
        </div>
        </div>
    }
}