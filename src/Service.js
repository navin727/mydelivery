import React from "react";
import { Link } from "react-router-dom";
export default class Service extends React.Component
{
    render()
    {
        return <div>
            <div className="container-xxl py-5">
        <div className="container py-5">
            <div className="text-center wow fadeInUp" data-wow-delay="0.1s">
                <h6 className="text-secondary text-uppercase">Our Services</h6>
                <h1 className="mb-5">Explore Our Services</h1>
            </div>
            <div className="row g-4">
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="https://rightcabs.com/wp-content/uploads/2018/09/piaggio-ape-cargo-auto-250x250-1-200x150.jpg" alt=""/>
                        </div>
                        <h4 className="mb-3">Tempo</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.5s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiD96NwEM5_f8OtCYwSz21COJu2jUiPp4q3Q&usqp=CAU" alt=""/>
                        </div>
                        <h4 className="mb-3">Eicher Mini</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.7s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="https://images.livemint.com/img/2020/08/13/600x338/press-13aug20-lowres_1597332021367_1597332027886.jpg"    alt="" height="200px" />
                        </div>
                        <h4 className="mb-3">Tata Motor Mint</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="https://img3.exportersindia.com/product_images/bc-full/dir_177/5285999/tempo-transportation-services-1508139129-3398940.jpeg" alt=""/>
                        </div>
                        <h4 className="mb-3">Eicher Truck</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.5s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="https://thumbs.dreamstime.com/z/cargo-truck-1-214234.jpg" alt=""/>
                        </div>
                        <h4 className="mb-3">Cargo Truck</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.7s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="https://images.unsplash.com/photo-1591768793355-74d04bb6608f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8dHJ1Y2t8ZW58MHx8MHx8&w=1000&q=80" alt=""/>
                        </div>
                        <h4 className="mb-3">Chassis Cab Truck</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
            </div>
            <br/>
            <div className="row g-4">
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="img/service-1.jpg" alt=""/>
                        </div>
                        <h4 className="mb-3">Air Freight</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.5s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="img/service-2.jpg" alt=""/>
                        </div>
                        <h4 className="mb-3">Ocean Freight</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.7s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="img/service-3.jpg" alt=""/>
                        </div>
                        <h4 className="mb-3">Road Freight</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="img/service-4.jpg" alt=""/>
                        </div>
                        <h4 className="mb-3">Train Freight</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.5s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="img/service-5.jpg" alt=""/>
                        </div>
                        <h4 className="mb-3">Customs Clearance</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
                <div className="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay="0.7s">
                    <div className="service-item p-4">
                        <div className="overflow-hidden mb-4">
                            <img className="img-fluid" src="img/service-6.jpg" alt=""/>
                        </div>
                        <h4 className="mb-3">Warehouse Solutions</h4>
                        <p>Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.</p>
                        <Link className="btn-slide mt-2" to="/price"><i className="fa fa-arrow-right"></i><span>Read More</span></Link>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    }
}