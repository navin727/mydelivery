import React from "react";
import AdminSlider from "./AdminSlider";
export default class VehicleList extends React.Component {
    render() {
        return <div>
            <div className="container-fluid bg-secondary min-vh-100">
                <div className="row">
                    <div className="col-2 bg-white vh-100">
                        <AdminSlider />
                    </div>
                    <div className="col-10 ">

                        <h1 style={{ background: '#8a0dfd17',color:'white', textAlign: 'center', padding: '20px', }}>Add Vehicle</h1>
                        <div className="card" style={{ width: 'auto', marginLeft: '10px' }}>
                            <div className="card-body">
                                <form className="form-group">
                                    <div className="row mt-3 mx-5">

                                        <div className="col-xl-6 col-lg-6">
                                            <input type="text" className="form-control" placeholder="Vehicle Name" /></div>
                                        <div className="col-xl-6 col-lg-6">
                                            <input type="text" className="form-control" placeholder="Loading Capacity" />
                                        </div>
                                    </div><br />
                                    <div className="row mx-5">
                                        <div className="col-xl-6 col-lg-6">
                                            <input type="text" className="form-control" placeholder="Price/Km" /></div>
                                        <div className="col-xl-6 col-lg-6">
                                            <input type="text" className="form-control" placeholder="Vehicle Number" />
                                        </div>
                                    </div>
                                    <br />
                                    <div className="row text-center">
                                        <div className="col-xl-12 col-lg-12">
                                            <button className="btn btn-success">Add Vehicle</button>
                                        </div></div>
                                </form>
                            </div>
                        </div>
                        <hr />

                        <table class="table  bg-white rounded">
                            <thead>
                                <tr>
                                    <th scope="col">S.No</th>
                                    <th scope="col">Vehicle Id</th>
                                    <th scope="col">Vehicle Name</th>
                                    <th scope="col">Loading Capacity</th>
                                    <th scope="col">Price/Km</th>
                                    <th scope="col">Vehicle Number</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>101</td>
                                    <td>Tempo</td>
                                    <td>500kg</td>
                                    <td>49Rs</td>
                                    <td>UP96-8085</td>
                                    <td><img src="https://www.shutterstock.com/image-illustration/pickup-mini-truck-carrier-3d-260nw-2187561257.jpg" height='100' width='200' /></td>
                                    <td><button className="btn btn-danger">Remove</button></td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>102</td>
                                    <td>Tata Intra</td>
                                    <td>400kg</td>
                                    <td>49Rs</td>
                                    <td>MP96-8086</td>
                                    <td><img src="https://intra.tatamotors.com/images/compact-trucks/tata-intra-trucks/tata-intra-v10-trucks/know-your-tata-intra/tata-intra-v10.png" height='100' width='200' /></td>
                                    <td><button className="btn btn-danger">Remove</button></td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>103</td>
                                    <td>Cargo</td>
                                    <td>1000kg</td>
                                    <td>99Rs</td>
                                    <td>MP09-8087</td>
                                    <td><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQeDHlnQposkbaZ6pehMokXkoS6DCQyPKCTWLG_8gFa&s" height='100' width='200' /></td>
                                    <td><button className="btn btn-danger">Remove</button></td>
                                </tr>
                                <tr>
                                    <th scope="row">4</th>
                                    <td>104</td>
                                    <td>Prima</td>
                                    <td>1000kg</td>
                                    <td>149Rs</td>
                                    <td>UP86-8086</td>
                                    <td><img src="https://cvimg1.cardekho.com/p/630x420/in/tata/prima-lx-3123tts/tata-prima-lx-3123tts.jpg?imwidth=480&impolicy=resize" height='100' width='200' /></td>
                                    <td><button className="btn btn-danger">Remove</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    }
}